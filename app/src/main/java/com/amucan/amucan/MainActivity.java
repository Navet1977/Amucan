package com.amucan.amucan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {


    //Declare instatnces
    private FirebaseAuth mAuth;
    private TextView register;
    private EditText email,password;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Initialize instances
        mAuth = FirebaseAuth.getInstance();

        register = (TextView) findViewById(R.id.textView3);
        email = (EditText) findViewById(R.id.editText_email_login);
        password = (EditText) findViewById(R.id.editText_password_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);


        /**
         * Listener for the Edit Text to create a new account
         */
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent new_register = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(new_register);

            }
        });

        /**
         * Listener for the Button for login with email and password
         */
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validate that the email and password don't came null's or empty's
                if (!email.getText().toString().isEmpty() || !email.getText().toString().equals(' ') && email.getTextSize() > 4){
                    loginWithEmailAndPassword(email.getText().toString(),password.getText().toString());
                }

            }
        });



    }

    /**
     * Try to SigIn with the email and password on Firebase
     * @param email String Contains the email from the user
     * @param passsword String Contains the password from the user
     */
    private void  loginWithEmailAndPassword(String email, String passsword){
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email,passsword).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    //Declare new Intent
                    Intent homeActivity = new Intent(MainActivity.this,HomeActivity.class);
                    //Start new Intent
                    try{
                        startActivity(homeActivity);
                    }catch (Exception exception){
                        Toast.makeText(MainActivity.this,"Error: "+exception,Toast.LENGTH_LONG).show();
                    }

                    //Finish this activity
                    finish();
                }else{

                    Toast.makeText(MainActivity.this,"Error al iniciar sesión : "+task.getException().toString() ,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
