package com.amucan.amucan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {


    //Declare instances
    private FirebaseAuth mAuth;
    private Button btnRegister;
    private EditText email;
    private EditText password;
    private EditText confirmation_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Initialize instances
        btnRegister = (Button) findViewById(R.id.btnRegister);
        email = (EditText) findViewById(R.id.editText_register_email);
        password = (EditText) findViewById(R.id.editText_register_password);
        confirmation_password = (EditText) findViewById(R.id.editText_register_confirmation_password);


        mAuth = FirebaseAuth.getInstance();


        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (password.getText().toString().equals(confirmation_password.getText().toString())){
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(RegisterActivity.this,"Usuario creado",Toast.LENGTH_LONG).show();
                                        Intent homeIntent = new Intent(RegisterActivity.this,HomeActivity.class);
                                        try{
                                            startActivity(homeIntent);
                                            finish();
                                        }catch (Exception ex){
                                            Toast.makeText(getApplicationContext(),"Error: " + ex,Toast.LENGTH_LONG).show();
                                        }
                                    } else {

                                        Toast.makeText(RegisterActivity.this,"Error al crear el usuario",Toast.LENGTH_LONG).show();

                                    }

                                }
                            });
                }
            }
        });



    }
}
